# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
### BEGIN LICENSE
# Copyright (C) 2012 David Planella <david.planella@ubuntu.com>
# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.
### END LICENSE

from gi.repository import Gtk, GtkChamplain, Clutter, Champlain
from qreator_lib.helpers import get_data_file
from gi.repository import GtkClutter


class QRCodeLocationGtk(object):
    def __init__(self, qr_code_update_func):
        self.qr_code_update_func = qr_code_update_func
        self.builder = Gtk.Builder()

        self.builder.add_from_file(
            get_data_file('ui', '%s.ui' % ('QrCodeLocation',)))
        self.grid = self.builder.get_object('qr_code_location')

        GtkClutter.init([])
        map_widget = GtkChamplain.Embed()
        map_widget.set_hexpand(True)
        map_widget.set_vexpand(True)

        map_grid = self.grid

        #map_grid.set_size_request(-1, 350)
        map_grid.attach(map_widget, 0, 0, 1, 1)

        self.map_view = map_widget.get_view()
        self.map_view.set_reactive(True)
        map_widget.connect('button-release-event',
                           self.on_map_widget_button_press_event,
                           self.map_view)

        # Get the current location, center the map on it, and initialize
        # other map features
        latitude, longitude = get_current_location()
        self.builder.get_object('lat_entry').set_text(str(latitude))
        self.builder.get_object('lon_entry').set_text(str(longitude))
        self.map_view.center_on(latitude, longitude)
        if latitude == 0 and longitude == 0:
            # In case something went wrong in getting the current location
            self.map_view.set_zoom_level(1)
        else:
            self.map_view.set_zoom_level(15)
        self.map_view.set_kinetic_mode(True)

        scale = Champlain.Scale()
        scale.connect_view(self.map_view)
        self.map_view.bin_layout_add(scale, Clutter.BinAlignment.START,
                                     Clutter.BinAlignment.END)

        self.grid.show_all()

    def on_activated(self):
        pass

    def on_prepared(self):
        pass

    def on_map_widget_button_press_event(self, actor, event, view):
        x, y = event.get_coords()
        lon, lat = view.x_to_longitude(x), view.y_to_latitude(y)

        self.builder.get_object('lat_entry').set_text(str(lat))
        self.builder.get_object('lon_entry').set_text(str(lon))

        self.qr_code_update_func('geo:{0},{1}'.format(str(lat), str(lon)))

        return True


def get_current_location():
    '''Gets the current location from geolocation via IP (only method
       currently supported)'''
    #import Geoclue
    #POS_PROVIDER = 'Ubuntu GeoIP'
    #location = Geoclue.DiscoverLocation()
    #location.init()
    #location.set_position_provider(POS_PROVIDER)
    #position = location.get_location_info()

    import dbus
    bus = dbus.SessionBus()

    # For now we default to the UbuntuGeoIP provider and we fall back to
    # Hostip. We should probably be cleverer about provider detection, but
    # this solution works for now and does not rely solely on UbuntuGeoIP,
    # which means qreator can run on other distros
    try:
        geoclue = bus.get_object(
            'org.freedesktop.Geoclue.Providers.UbuntuGeoIP',
            '/org/freedesktop/Geoclue/Providers/UbuntuGeoIP')
    except dbus.exceptions.DBusException:
        geoclue = bus.get_object(
            'org.freedesktop.Geoclue.Providers.Hostip',
            '/org/freedesktop/Geoclue/Providers/Hostip')
    position_info = geoclue.GetPosition(
        dbus_interface='org.freedesktop.Geoclue.Position')

    position = {}
    position['timestamp'] = position_info[1]
    position['latitude'] = position_info[2]
    position['longitude'] = position_info[3]
    position['altitude'] = position_info[4]

    return position['latitude'], position['longitude']
